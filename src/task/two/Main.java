/**
 * 2) Описать класс Треугольник. В качестве свойств возьмите длины сторон треугольника.
 * Реализуйте метод, который будет возвращать площадь этого треугольника.
 * Создайте несколько объектов этого класса и протестируйте их.
 */

package task.two;

public class Main {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(0.3, 0.4, 0.6);
        Triangle triangle2 = new Triangle(3, 5, 6);
        Triangle triangle3 = new Triangle(0.5, 0.4, 0.7);

        System.out.println("Triangle 1 area = " + triangle1.getArea());
        System.out.println("Triangle 2 area = " + triangle2.getArea());
        System.out.println("Triangle 3 area = " + triangle3.getArea());
    }
}
