package additional.task.one;

public class Phone {
    private String number;
    private String manufacturer;
    private String model;
    private Network network;

    public Phone() {
        super();
    }

    public Phone(String number, String manufacturer, String model) {
        super();
        this.number = number;
        this.manufacturer = manufacturer;
        this.model = model;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }

    public void registerInNetwork(Network network) {
        network.addPhone(this);
        this.setNetwork(network);
    }

    public void makeCall(String phoneNumber) {
        if (this.network == null) {
            System.out.println("Your phone number is not registered in any network.");
            return;
        }
        if (this.number.equals(phoneNumber)) {
            System.out.println("You can`t call to yourself.");
            return;
        }

        Phone contactPhone = this.network.getPhoneByNumber(phoneNumber);
        if (contactPhone == null) {
            System.out.println("Phone number is not registered.");
            return;
        }

        contactPhone.receiveCall(this);
    }

    public void receiveCall(Phone phone) {
        System.out.println("Calling " + phone.getNumber() + " from network " + phone.getNetwork().getName());
    }

    @Override
    public String toString() {
        return "Phone{" +
                "number='" + number + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
