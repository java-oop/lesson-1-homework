package additional.task.one;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Network {
    private static List<Phone> phones;
    private String name;

    public Network() {
        super();
        phones = new ArrayList<>();
    }

    public Network(String name) {
        this.name = name;
        phones = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addPhone(Phone phone) {
        phones.add(phone);
    }

    public Phone getPhoneByNumber(String number) {
        for (Phone phone: phones) {
            if (phone.getNumber().equals(number)) {
                return phone;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return "Network{" +
                "phones=" + Arrays.toString(phones.toArray()) +
                ", name='" + name + '\'' +
                '}';
    }
}
